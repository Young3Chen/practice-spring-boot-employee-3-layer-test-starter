package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeSalaryException;
import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class EmployeeServiceTest {
    private EmployeeRepository employeeRepository;
    private EmployeeService employeeService;

    @BeforeEach
    void setUp() {
        employeeRepository = Mockito.mock(EmployeeRepository.class);
        employeeService = new EmployeeService(employeeRepository);
    }

    @Test
    void should_throw_age_Exception_when_insert_given_age15_and_age66() {
        //given
        Employee employeeYoung = new Employee(1, "Young", 15, "male", 20000);
        Employee employeeOld = new Employee(2, "Old", 66, "Female", 2000011);

        //when then
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.getInsert(employeeYoung));
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.getInsert(employeeOld));
        Mockito.verify(employeeRepository, Mockito.times(0)).insert(any());
    }

    @Test
    void should_throw_age_Exception_when_insert_given_age_35_and_salary_18000() {
        //given
        Employee employee = new Employee(1, "Employee", 35, "male", 18000);

        //when then
        Assertions.assertThrows(AgeSalaryException.class, () -> employeeService.getInsert(employee));
        Mockito.verify(employeeRepository, Mockito.times(0)).insert(any());
    }

    @Test
    void should_validate_insert_function_with_status_true_when_insert_employee_given_a_employee_match_rule() {
        //given
        Employee employee = new Employee(1, "lisi", 33, "Man", 36000);

        //when
        employeeService.getInsert(employee);

        //then
        Mockito.verify(employeeRepository).insert(argThat(savedEmployee -> {
            assertTrue(savedEmployee.getStatus());
            return true;
        }));
    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_a_employee_match_rule() {
        //given
        Employee employee = new Employee(1, "lisi", 33, "Man", 36000);
        Employee employeeSaved = new Employee(1, "lisi", 33, "Man", 36000);
        employeeSaved.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeSaved);

        //when
        Employee insertEmployee = employeeService.getInsert(employee);

        //then
        Assertions.assertTrue(insertEmployee.getStatus());
    }

    @Test
    void should_delete_employee_set_status_false_when_insert_employee_given_a_savedEmployee_id() {
        //given
        Employee employeeSaved = new Employee(1, "lisi", 33, "Man", 36000);
        when(employeeRepository.findById(anyInt())).thenReturn(employeeSaved);

        //when
        employeeService.getDelete(employeeSaved.getId());

        //then
        Mockito.verify(employeeRepository).update(argThat(employee -> {
            assertFalse(employee.getStatus());
            return true;
        }));
    }

    @Test
    void should_throw_NoFoundException_exception_employee_when_updateEmployee_given_active_employee() {
        //given
        Employee employeeUpdated = new Employee(1, "lili", 33, "man", 12000);

        //when then
        Assertions.assertThrows(NotFoundException.class,
                () -> employeeService.getUpdate(employeeUpdated.getId(), employeeUpdated));
        Mockito.verify(employeeRepository, Mockito.times(0)).updateById(anyInt(), any());
    }
}