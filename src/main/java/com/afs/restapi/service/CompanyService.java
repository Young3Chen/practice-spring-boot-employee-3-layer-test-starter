package com.afs.restapi.service;

import com.afs.restapi.controller.CompanyController;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getList() {
        return companyRepository.getCompanies();
    }

    public Company getCompany(Integer companyId) {
        return companyRepository.getCompany(companyId);
    }

    public List<Employee> getEmployees(Integer companyId, CompanyController companyController) {
        return companyRepository.getEmployees(companyId);
    }

    public List<Company> getCompanyListPage(Integer pageIndex, Integer pageSize, CompanyController companyController) {
        return companyRepository.getCompanyListPage(pageIndex, pageSize);
    }

    public Company updateCompanyById(Integer companyId, Company company, CompanyController companyController) {
        return companyRepository.getCompany(companyId, company, this);
    }

    public void removeCompanyById(Integer companyId) {
        companyRepository.removeCompanyById(companyId);
    }

}