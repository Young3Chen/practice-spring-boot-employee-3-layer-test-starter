package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeSalaryException;
import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee getById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee getInsert(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 60) {
            throw new AgeErrorException();
        }
        if (employee.getAge() >= 30 && employee.getSalary() < 20000) {
            throw new AgeSalaryException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee getUpdate(int id, Employee employee) {
        Employee employeeFound = getById(id);
        if (employeeFound != null) {
            return employeeRepository.updateById(id, employee);
        }
        throw new NotFoundException();
    }

    public void getDelete(int id) {
        Employee employeeFound = getById(id);
        if (employeeFound != null) {
            employeeFound.setStatus(false);
            employeeRepository.update(employeeFound);
        }
    }
}