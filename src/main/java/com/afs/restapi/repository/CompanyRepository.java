package com.afs.restapi.repository;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.service.CompanyService;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    private static final List<Company> companies = new ArrayList<>();


    public CompanyRepository() {
        List<Employee> employeesInSpring = new ArrayList<>();
        employeesInSpring.add(new Employee(1, "alice", 21, "female", 6000));
        employeesInSpring.add(new Employee(2, "bob", 20, "male", 6200));
        employeesInSpring.add(new Employee(3, "charles", 22, "mfale", 5800));

        List<Employee> employeesInBoot = new ArrayList<>();
        employeesInBoot.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInBoot.add(new Employee(2, "ethan", 19, "male", 6000));

        companies.add(new Company(1, "spring", employeesInSpring));
        companies.add(new Company(2, "boot", employeesInBoot));
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void addCompany(Company company) {
        companies.add(new Company(generateNewId(), company.getCompanyName(), company.getEmployees()));
    }

    private int generateNewId() {
        int maxId = companies.stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }

    public Company getCompany(Integer companyId) {
        return getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployees(Integer companyId) {
        return getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .map(company -> company.getEmployees())
                .orElse(Collections.emptyList());
    }

    public List<Company> getCompanyListPage(Integer pageIndex, Integer pageSize) {
        return getCompanies().stream()
                .skip((pageIndex - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    private Company updateCompanyAttributes(Company companyStored, Company company) {
        if (company.getCompanyName() != null) {
            companyStored.setCompanyName(company.getCompanyName());
        }
        return companyStored;
    }

    public Company getCompany(Integer companyId, Company company, CompanyService companyService) {
        return getCompanies().stream()
                .filter(storedCompany -> storedCompany.getId().equals(companyId))
                .findFirst()
                .map(storedcompany -> updateCompanyAttributes(storedcompany, company))
                .orElse(null);
    }

    public boolean removeCompanyById(Integer companyId) {
        return getCompanies().removeIf(company -> company.getId().equals(companyId));
    }
}

