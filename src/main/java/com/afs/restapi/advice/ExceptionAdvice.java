package com.afs.restapi.advice;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeSalaryException;
import com.afs.restapi.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler({NotFoundException.class,})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessageResponse exceptionHandler(Exception e) {
        return new ErrorMessageResponse("404", e.getMessage());
    }

    @ExceptionHandler({AgeErrorException.class, AgeSalaryException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessageResponse paramErrorHandler(Exception e) {
        return new ErrorMessageResponse("400", e.getMessage());
    }
}
